//26. ( ** ) Дано двовимірний масив з 𝑁 × 𝑁 цілих чисел. Помножити матрицю саму на себе (відповідно до правил множення матриць).

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 3

int main()
{
	int i, j, k;
	int A[3][3] = {};
	int B[3][3] = {};
	int C[3][3] = {};

	srand(time(NULL));
	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++) {
			A[i][j] = rand() %
				  10; //создаем матрицу из рандомных чисел
			B[i][j] = A[i][j];
		}
	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++) {
			C[i][j] = 0;
			for (k = 0; k < N; k++)
				C[i][j] +=
					A[i][k] *
					B[k]
					 [j]; // перемножаем матрицы по правилу умножения матриц
		}
	return 0;
}
